# Cidinet test front-ned

_Front end de la solucion_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._




### Pre-requisitos 📋

_Necesitas tenes node.js en tu maquina local_


### Instalación 🔧


_Clona el proyecto en tu maquina local_

```
git clone https://gitlab.com/manuelkiki16/cidinet-test-front

```

_ve a la carpeta creada e instala las dependencias_

```
cd nombrecarpeta && npm install
```

### Si cambiaste el puerto donde corre el api deberas hacer este paso extra

_Abre el proyecto con un editor de texto y edita el siguiente archivo /src/net/index.ts solo basta con cambiar el puerto en la url_

_Finalmente corre el proyecto_
```
npm start

```


## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [React.js](https://es.reactjs.org/) - React.js
* [typescript](https://www.typescriptlang.org/) - Typescript
* [AntDesing](https://ant.design/) - Ant Desing
