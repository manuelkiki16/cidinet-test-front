import {createStore} from "redux";
import EmployeReducer from "./employe.reducer";

export default createStore(EmployeReducer);