import {HomeList} from "../pages/HomeList";
import {AddEmploye} from "../pages/AddEmploye";
import {EditEmploye} from "../pages/EditEmploye";

export const routes = [
    {
      name:"HomePage",
      path: "/",
      component: HomeList
    },
    {
      name:"AddEmploye",
      path: "/AddEmploye",
      component: AddEmploye
    },
    {
      name:"EditEmploye",
      path: "/EditEmploye/:employeId",
      component: EditEmploye
    }
  ];

