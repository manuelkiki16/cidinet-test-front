export const DOMAIN_EMAIL='cidenet.com.us';
export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_TIME_FORMAT = 'YYYY-MM-DD hh:mm:ss'
export const EMPLOYE_FORM_OBJECT = {



}
export const TYPES_IDS = {
    "CC":"Cédula de Ciudadanía",
    "CE":"Cédula de Extranjería",
    "PP":"Pasaporte",
    "PE":"Permiso Especial"
}
export const COUNTRYES = {
     COL:"Colombia",
     EU:"Estados Unidos"
}
export const AREAS_P = {
 ADM:"Administración",
 FN:"Financiera",
 CMP:"Compras",
 INF:"Infraestructura",
 OP:"Operación",
 TH:"Talento Humano",
 SV:"Servicios Varios"
}
export const DATA_FILTER = {
  employeFirstName:"Primer nombre",
  employeOtherName:"Otros nombres",
  employeFirstSurname:"Primer Apellido",
  employeSecondSurname:"Segundo Apellido",
  employeIdType:"Tipo de identificacion",
  employeNumberId:"Numero de identificacion",
  employeCountry:"Pais",
  employeStatus:"Estado"
}

const typeTemplate = "El campo no es un ${type} valido";
export const Patterns = {
    OnlyText:new RegExp(/^[a-zA-Z ]+$/,'g'),
    Alphanumeric:/^[A-Za-z0-9-]+$/,


}

export const DEFAULT_VALIDATE_MSG = {
  default: "error al validar El campo",
  required: "El campo es requerido",
  enum: "El campo debe ser alguno de  [${enum}]",
  whitespace: "El campo No puede estar vacio",
  date: {
    format: "El campo tiene un formato ni valido",
    parse: "El campo no se pudo pasar a fecha",
    invalid: "El campo no es una fecha ",
  },
  types: {
    string: typeTemplate,
    method: typeTemplate,
    array: typeTemplate,
    object: typeTemplate,
    number: typeTemplate,
    date: typeTemplate,
    boolean: typeTemplate,
    integer: typeTemplate,
    float: typeTemplate,
    regexp: typeTemplate,
    email: typeTemplate,
    url: typeTemplate,
    hex: typeTemplate,
  },
  string: {
    len: "El campo debe contener ${len} caracteres",
    min: "El campo debe contener al menos ${min} caracteres",
    max: "El campo no puede contener mas de  ${max} caracteres",
    range: "El campo debe estar entre ${min} y ${max} caracteres",
  },
  number: {
    len: "El campo debe ser igual a ${len}",
    min: "El campo no puede ser menor a ${min}",
    max: "El campo no puede ser mayor a ${max}",
    range: "El campo debe estar entre ${min} y ${max}",
  },
  array: {
    len: "El campo debe ser exactamente ${len} en tamaño",
    min: "El campo cannot be less than ${min} in length",
    max: "El campo cannot be greater than ${max} in length",
    range: "El campo must be between ${min} and ${max} in length",
  },
  pattern: {
    mismatch: "El campo no coincide con el patron ${pattern}",
  },
};