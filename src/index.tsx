import React from 'react';
import ReactDOM from 'react-dom';
import "antd/dist/antd.css";
import './assets/css/custom.css';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import Store from "./store/store";
import {routes} from "./routes/index";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

 

ReactDOM.render(
  <Provider store={Store} >
  <Router>

  <Switch>
        
        {
          routes.map( (ele,index)=>
            <Route exact={true} key={index.toString()} path={ele.path} >
                <ele.component />
            </Route>
          )
        }
        

      </Switch>

  </Router>
  
</Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
