import React,{FC,useState} from "react";
import { Container, Row, Col } from 'react-grid-system';
import {Form,Input,Select,DatePicker, Button} from "antd";
import { DOMAIN_EMAIL,DEFAULT_VALIDATE_MSG, DATE_FORMAT, Patterns, TYPES_IDS,COUNTRYES,AREAS_P } from "../utils/contants";
import {useHistory} from "react-router-dom";
import  moment from "moment";
type EmployeForm = {
  typeAction:string,
  onSendAction:any,
  employeObject:any,
  employeIdType:string,
  setEmployeIdType:any,
  employeEmail:string,
  setEmployeEmail:any,
  onChangeFormValue:any
 
}

export const FormEmploye : FC< EmployeForm > = (props)=>{


    const [defaultData,setDefaultData]  = useState({} as any);
    let history = useHistory();
    const [form] = Form.useForm();
   
    React.useEffect(()=>{

      
        setDefaultData(props.employeObject);
        console.log(props.employeObject);


    },[props.employeObject]);

 

    
    

    const DisabledDate = (current:any)=>{
      

        

        return  current < moment().add(-1,"M") || current >  moment();
    }

    const onFinish = (values: any) => {
        values.employeDateIn  = moment(values.employeDateIn).format("YYYY-MM-DD");
        props.onSendAction(values);
      };
    

    return (
        <Row>
            <Col sm={12} xs={12} md={12}  >
                
                {

               

              ( props.typeAction === 'ADD' || Object.keys(defaultData).length > 0 )
                 &&    
                 <Form  onValuesChange={props.onChangeFormValue}  form={form}   initialValues={props.employeObject} onFinish={onFinish}  validateMessages={DEFAULT_VALIDATE_MSG} layout="vertical" >

                 <Row>
                     <Col sm={6} xs={6} md={3} >
                         <Form.Item
                         name='employeFirstName'
                         
                         rules={[{ required: true,pattern:Patterns.OnlyText,max:20}]}
                         label="Primer nombre"  tooltip="este campo es requerido">
                             <Input type="text"  />
                         </Form.Item>
                         
                     </Col>
                     <Col sm={6} xs={6} md={3} >
                         <Form.Item 
                         label="Otros nombres" 
                         name='employeOtherName'
                         rules={[{ required: false,pattern:Patterns.OnlyText,max:50}]}
                         tooltip="este campo es requerido">
                         <Input 
                         placeholder=""
                         
                         />
                         </Form.Item>
                         
                     </Col>
                     <Col sm={6} xs={6} md={3} >
                         <Form.Item 
                         label="Primer apellido" 
                         name='employeFirstSurname'
                         rules={[{ required: true,pattern:Patterns.OnlyText,max:20}]}
                         required 
                         tooltip="este campo es requerido">
                             <Input 
                              
                                 placeholder="" />
                         </Form.Item>
                         
                     </Col>
                     <Col sm={6} xs={6} md={3} >
                         <Form.Item 
                         label="Segundo apellido" 
                         name='employeSecondSurname'
                         rules={[{ required: true,pattern:Patterns.OnlyText,max:20}]}
                         required 
                         tooltip="este campo es requerido">
                             <Input 
                             placeholder=""
                         
                             />
                         </Form.Item>
                         
                     </Col>
                     <Col sm={12} xs={12} md={3} >
                         <Form.Item  
                         label="Pais" 
                         rules={[{ required: true}]}
                         name='employeCountry'
                         tooltip="este campo es requerido">
                         <Select  
                        
                         >
                          
                            {
                                Object.keys(COUNTRYES).map( (ele,i)=>
                                <Select.Option key={i.toString()} value={ele} >{ Object.values(COUNTRYES)[i] }</Select.Option>
                                )
                            }
 
                         </Select>
                         </Form.Item>
                     </Col>
                     <Col sm={12} xs={12} md={4} >
                         <Form.Item  
                         rules={[{ required: true,pattern:Patterns.Alphanumeric,max:20}]}
                         label="Documento de identidad" 
                         required 
                         name='employeNumberId'
                         tooltip="este campo es requerido">
                         <Input addonBefore={
                                <Select 
                                onChange={(value)=>props.setEmployeIdType(value)}
                                defaultValue={defaultData.employeIdType || props.employeIdType}
                                 style={{width:140}} >
                                    {
                                        Object.keys(TYPES_IDS).map((ele:string,index:number)=>
                                        <Select.Option key={index.toString()} value={ele  } >{Object.values(TYPES_IDS)[index]}  </Select.Option>
                                        )
                                    }
                                     
                                    
                                </Select>
                             }  style={{ width: '100%' }}  />
                         </Form.Item>
                         
                     </Col>
                     <Col sm={12} xs={12} md={4} >
                     <Form.Item  
                     
                     name='employeEmail'
                     label="Correo electronico" 
                     required tooltip="este campo es requerido"
                     
                     >
                        <Input.Group compact>
                           <Input 
                           value={props.employeEmail}
                            readOnly  
                            addonAfter={DOMAIN_EMAIL} placeholder="examplo.exam"
                            />
                        </Input.Group>
                        
                     </Form.Item>
 
                     </Col>
                     <Col sm={12} xs={12} md={3} >
                     <Form.Item   
                     name='employeDateIn'
                     rules={[{ required: true,type:"date"}]}
                     label="Fecha de ingreso" 
                     required 
                     tooltip="este campo es requerido">
                         <DatePicker 
                         disabledDate={(current)=>DisabledDate(current) }
                         format={DATE_FORMAT} style={{width:'100%'}}   onChange={()=>{}} />
                     </Form.Item>
 
                     </Col>
                     <Col sm={12} xs={12} md={3} >
                         <Form.Item  
                         name='employeArea' 
                         rules={[{ required: true}]}
                         label="Area" 
                         required tooltip="este campo es requerido">
                             <Select  style={{ width: '100%' }} >
                                 {
                                     Object.keys(AREAS_P).map( (ele,i)=>
                                     <Select.Option key={i.toString()} value={ele} >{Object.values(AREAS_P)[i]}</Select.Option>
                                     )
                                 }
                                
                             </Select>
                         </Form.Item>
                     </Col>
                     <Col sm={12} xs={12} md={3} >
                         <Form.Item  
                         name='employeStatus' 
                         label="Estado" 
                         >
                             <Select defaultValue={1} disabled  style={{ width: '100%' }} >
                                 <Select.Option value="1">Activo</Select.Option>
                                 <Select.Option value="0">Inactivo</Select.Option>
                             </Select>
                         </Form.Item>
                     </Col>
                    {
                        defaultData.emplayeDateCreated &&
                         <Col sm={12} xs={12} md={3} >
                         <Form.Item  
                         name='emplayeDateCreated'
                         label="Fecha de creacion"  tooltip="Campo inhabilitado">
                            <Input readOnly  />
                         </Form.Item>
                     </Col>
                    }
                     <Col sm={12} xs={12} md={12} >
                         <Row justify="end" align="end"  >
                             <Col  xs={12} sm={12} md={12} >
                                 <Button htmlType="submit"  style={{float:"right"}} size="large" type="primary" > {props.typeAction =='ADD'? 'Registrar':'Editar'  } empleado</Button>
                                 <Button danger htmlType="button" onClick={evt=>history.push("/")}  style={{float:"right",marginRight:10}} size="large" color="" type="primary" > Regresar al listado</Button>
                             </Col>
                         </Row>
                     </Col>
                     
 
                 </Row>
             
 
                 </Form>
 
             }

            </Col>
            
        </Row>
    
    )


}