import React,{FC} from "react";
import { Container, Row, Col } from 'react-grid-system';
import { Select,Input } from 'antd';


const { Option } = Select;


type SearchProps = {
   onChangeTypeFilter:any,
   onChangeFilterValue:any,
   DataSetSearchFilter:any
}

export const SearchBar: FC<SearchProps> = (props)=>{
    
    function handleChange(value:string) {
        console.log(`selected ${value}`);
      }

return (
<div className='search-container' >
        <Container>
            <Row>
                <Col sm={12} md={12}  >
                    <Input.Group compact>
                        <Select onChange={ (value:string)=>props.onChangeTypeFilter(value) }  className='select-filter'  style={{ width: '40%' }} defaultValue={props.DataSetSearchFilter.keys[0]}>
                            {
                             props.DataSetSearchFilter.keys.map( (ele:string,i:number)=>
                             <Option
                             key={i.toString()}
                             value={ele}>{props.DataSetSearchFilter.values[i]}</Option>
                             ) 
                            }
                         
                          
                        </Select>
                        <Input.Search onChange={evt=>props.onChangeFilterValue(evt.target.value)} className='searc-input-head' style={{ width: '60%' }} placeholder="input search text" onSearch={()=>{}} enterButton />
                    </Input.Group>
                </Col>
               
            </Row>
        </Container>
</div>
)

}


