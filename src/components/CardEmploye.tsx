import React,{FC} from "react";
import {Tag,Typography} from "antd";
import { Container, Row, Col, } from 'react-grid-system';
import IcEdit from "../assets/img/ic_edit.png";
import IcDelete from "../assets/img/ic_delete.png";


type CardEmployeProps = {
    onEdit:any,
    onDropEmploye:any
    employeName:string,
    employeNumberId:string,
    employTypeId:string,
}
const CardEmploye: FC<CardEmployeProps> = (props)=>{


    return (
        <Col sm={12} xs={12} md={4} >
            <div className='card-employe' >
                <div className='card-employe-head' >
                    <Tag className='active-label' color="#1890ff">Activo</Tag>
                </div>
                <div className='content' >

                    <Row>
                        <Col sm={3} md={3} xs={3} >
                            <div className='card-employe-first-letter' >
                                <Typography.Text>{props.employeName[0].toUpperCase()}</Typography.Text>
                            </div>
                        </Col>
                        <Col sm={6} md={6} xs={6} >
                           <div className='card-employe-name-us' >
                            <Typography.Title level={4}  >{props.employeName}</Typography.Title>
                           </div>
                            <Typography.Text   >{props.employTypeId} {props.employeNumberId}</Typography.Text>
                        </Col>
                        <Col sm={3} md={3} xs={3} >
                            <div onClick={props.onDropEmploye} className='card-employe-drop-content' >
                                <img src={IcDelete}   />
                            </div>
                        </Col>
                       

                    </Row>
                    
                     
                </div>
                <div className='card-employe-footer' >
                        <div onClick={props.onEdit}  className='card-employe-btn-edit' >
                            <img src={IcEdit}   />
                        </div>
                </div>

            </div>

        
        </Col>
        )

}

export default CardEmploye;