import React,{useEffect,useState} from "react";
import {Typography,notification} from "antd";
import { Container, Row, Col } from 'react-grid-system';
import {FormEmploye} from "../components/FormEmploye";
import {CreateEmploye, GetSuggestMail} from "../net/employe";
import {useHistory} from "react-router-dom";


const AddEmploye = (props:any)=>{

     let [countrySelected,setCountrySelected ] = useState("");
     let history = useHistory();

     const[employeIdType,setEmployeIdType] = useState("CC");
     const[employeEmail,setEmployeEmail] = useState("");
   

     useEffect(()=>{
        
     },[]);

     const SendData = (data:any)=>{
   
    data.employeIdType = employeIdType;
    CreateEmploye(data).then((result:any) =>{

        if( result.data.status == 400 ){

            notification.open({
                message:"Advertencia",
                description:result.data.response.msg,
                type:"warning",
            });

        }else{

            notification.open({
                message:"Employe registered",
                description:result.data.msg,
                type:"success",
            });
            setTimeout(()=>{
    
                history.push("/");
    
            },1500);

        }

 


    }).catch((error:any)=>{

        console.log(error.response);
         if( error.response.data.statusCode == 400 ){
                notification.open({
                    message:"Error de registro",
                    description:"Errror al tratar de registrar el usuario"

                })
        } 

    });
    console.log("send data",data);

    }

    let OnChangeFormValues  = (changedValues:any, allValues:any)=>{

        let keysObj = Object.keys(changedValues);
        if( keysObj.indexOf("employeFirstName") !== -1 || keysObj.indexOf("employeFirstSurname") !== -1  ){

            if(  !allValues.employeFirstName || !allValues.employeFirstSurname ){
                return ;
            }
          
             GetSuggestMail({
                employeFirstName:allValues.employeFirstName,
                employeFirstSurname:allValues.employeFirstSurname
             }).then((result:any)=>{

                  setEmployeEmail(String(result.data.value).substring(0,result.data.value.lastIndexOf("@"))  );

             }).catch((err:any)=>{
  /*                   notification.open({
                        message:"Error",
                        description:err.message
                    }); */
             });

        }
           
    }

    return (
        <div  className='view-add-employe-content' >
                <div className='text-center add-employe-header-content' >
                    <Typography.Title level={2}  >Registrar nuevo empleado</Typography.Title>
                </div>
                <Container>
                    <FormEmploye 
                    typeAction='ADD'
                    onSendAction={SendData}
                    employeObject={{employeStatus:'1',employeIdType:"CC"}}
                    employeIdType={employeIdType}
                    setEmployeIdType={setEmployeIdType}
                    onChangeFormValue={OnChangeFormValues}
                    employeEmail={employeEmail}
                    setEmployeEmail={setEmployeEmail}
                  
                    />
                </Container>
                


        </div>
    )


}

export {AddEmploye};