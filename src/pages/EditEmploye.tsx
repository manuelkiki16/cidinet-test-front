import React,{useEffect,useState} from "react";
import {Typography,notification} from "antd";
import { Container, Row, Col } from 'react-grid-system';
import {FormEmploye} from "../components/FormEmploye";
import {EditEmploye as  ReqEditEmploye,GetEmployeById,GetSuggestMail} from "../net/employe";
import {useHistory,useParams} from "react-router-dom";
import moment from "moment";
import shallowequal from "shallowequal";
import { DATE_FORMAT } from "../utils/contants";


const EditEmploye = (props:any)=>{

     let {employeId} = useParams() as any ;

     let history = useHistory();

     const[employeIdType,setEmployeIdType] = useState("CC");
     const [employeObject,setEmployeObjet] = useState({} as any );
     const [employeEmail,setEmployeEmail] = useState('');


     useEffect(()=>{
         

        GetEmployeById(employeId).then(result=>{
            result.data.value.employeDateIn = moment(result.data.value.employeDateIn);
            result.data.value.emplayeDateCreated = moment(result.data.value.emplayeDateCreated).format("YYYY-MM-DD hh:mm:ss");
            result.data.value.employeStatus = result.data.value.employeStatus?'1':'0';
            result.data.value.employeEmail = String(result.data.value.employeEmail ).substring(0,result.data.value.employeEmail.lastIndexOf("@"));
            setEmployeEmail(result.data.value.employeEmail);
            setEmployeObjet(result.data.value);
            setEmployeIdType(result.data.value.employeIdType);

             
        }).catch(err=>{

        });
        
     },[]);

     const SendData = (data:any)=>{
   
    data.employeIdType = employeIdType;
    let compareObj = {...employeObject};
    compareObj.employeDateIn = moment(compareObj.employeDateIn).format(DATE_FORMAT);
    let employeId = compareObj.employeId;
    delete compareObj.employeId;
   if( shallowequal(data,compareObj) ){
    notification.open({
        message:"Advertencia",
        description:"Debe realizar cambios en los datos si quiere actualizar el regsitro",
        type:"warning",
    });
    return ;
   }
   data.employeId = employeId;
    ReqEditEmploye(data).then((result:any)=>{

        if( result.data.status == 400 ){

            notification.open({
                message:"Advertencia",
                description:result.data.response.msg,
                type:"warning",
            });

        }else{

            notification.open({
                message:"Registro editado",
                description:"EL empleado ha sido editado exitosamente",
                type:"success",
            });
            setTimeout(()=>{
    
                history.push("/");
    
            },1500);

        }

 


    }).catch(error=>{

        console.log(error);
         if( error.response.data.statusCode == 400 ){
                notification.open({
                    message:"Error de registro",
                    description:"Errror al tratar de registrar el usuario"

                })
        } 

    });
    console.log("send data",data);

    }

    let OnChangeFormValues  = (changedValues:any, allValues:any)=>{

        let keysObj = Object.keys(changedValues);
        if( keysObj.indexOf("employeFirstName") !== -1 || keysObj.indexOf("employeFirstSurname") !== -1  ){
          
             GetSuggestMail({
                employeFirstName:allValues.employeFirstName,
                employeFirstSurname:allValues.employeFirstSurname
             }).then((result:any)=>{

                  setEmployeEmail(String(result.data.value).substring(0,result.data.value.lastIndexOf("@"))  );

             }).catch((err:any)=>{
  /*                   notification.open({
                        message:"Error",
                        description:err.message
                    }); */
             });

        }
           
    }

    return (
        <div  className='view-add-employe-content' >
                <div className='text-center add-employe-header-content' >
                    <Typography.Title level={2}  >Editar empleado</Typography.Title>
                </div>
                <Container>
                    <FormEmploye 
                    typeAction='EDIT'
                    onSendAction={SendData}
                    employeObject={employeObject}
                    employeIdType={employeIdType}
                    setEmployeIdType={setEmployeIdType}
                    onChangeFormValue={OnChangeFormValues}
                    employeEmail={employeEmail}
                    setEmployeEmail={setEmployeEmail}
                   
                    />
                </Container>
                


        </div>
    )


}

export {EditEmploye};