import React,{useState,useEffect} from "react";
import {useSelector,useDispatch} from "react-redux";
import { Container, Row, Col } from 'react-grid-system';
import {SearchBar} from "../components/SearchBar";
import CardContainer from "../components/CardEmploye";
import CardEmploye from "../components/CardEmploye";
import {Button,notification,Pagination,Modal} from "antd";
import {useHistory} from "react-router-dom";
import {GetAllEmployes,DropEmploye} from "../net/employe";
import { DATA_FILTER } from "../utils/contants";

const DataSetSearchFilter = {
    keys:Object.keys(DATA_FILTER),
    values:Object.values(DATA_FILTER)
} as any;

export const HomeList = (props:any)=>{

    let history = useHistory();
    let [dataSet,setDataSet] = useState([]);
    let [displayData,setDisplayData] = useState([]);
    let [typeFilter,setTypeFilter] = useState(DataSetSearchFilter.keys[0]);
    let [searchValue,setSearchValue] = useState("");

    useEffect(()=>{

        GetAllEmployes().then((result:any)=>{

            setDataSet(result.data.value);
            SetPaginationData(1,10,result.data.value);


        }).catch((error:any)=>{

            notification.open({
                message:"Error",
                description:error.message
            });

        });


    },[]);

    const onSearchValue= (text:string)=>{
        setSearchValue(text);
        let result = dataSet.filter( (ele:any)=> String(ele[typeFilter]).toLocaleLowerCase().indexOf( text.toLowerCase() ) !== -1  );
        setDisplayData(result);
           
           
    }

    const DropUser = (employeId:number)=>{

        DropEmploye(employeId).then((result:any)=>{

            if( result.data.status == 400 ){

                notification.open({
                    message:"Error",
                    type:"error",
                    description:result.data.msg
                }); 
                return ;
            }

            notification.open({
                message:"Drop successfull",
                type:"success",
                description:result.data.msg
            }); 

        }).catch((error:any)=>{
            notification.open({
                type:"error",
                message:"Error",
                description:error.message
            });
        });

        setDataSet( dataSet.filter( (ele:any)=>ele.employeId !==employeId  ) );
        setDisplayData( displayData.filter( (ele:any)=>ele.employeId !==employeId  ) );

    }

    const OnDropEmploye = (employeId:number,employeName:string)=>{

        Modal.confirm({
            title:"Confirmar Borrado",
            content:`Esta seguro de borrar el usuario con el nombre ${employeName}`,
            okText:"SI, Borrar",
            cancelText:"NO, Cancelar",
            onOk:()=>DropUser(employeId)
        });


    }

    const SetPaginationData = (page:number,pageSIze:number,customData?:[])=>{
        let Init = (page-1) * pageSIze;
        let end = (pageSIze * page);
       let visibleArray =  customData?  customData.slice(Init,end) : dataSet.slice(Init,end);
       console.log({visibleArray});
       setDisplayData(visibleArray);

    }


   const OnChangePagination = (page:number,pageSIze:number| any)=>{
       console.log({page,pageSIze});
       SetPaginationData(page,pageSIze);

    }

    const GoToEditUser = (employeId:number)=>{
        history.push("/EditEmploye/"+employeId);
    }

    
    

    return (
       <>
       <Container>
           <Row>
            <Col  sm={1} xs={1} md={3} >
            </Col>
            <Col sm={12} xs={12} md={6} >
                <SearchBar
                onChangeFilterValue={onSearchValue}
                onChangeTypeFilter={setTypeFilter}
                DataSetSearchFilter={DataSetSearchFilter}
                />
            </Col>
            <Col sm={1} xs={1} md={3} >
            </Col>
            <Col  sm={12} xs={12} md={12} >
                <div className='actions-container' >
                    <Button onClick={e=>history.push("AddEmploye")} type="primary" size="large"  shape="round" >Agregar nuevo empleado</Button>
                </div>
                <div className='list-employe' >
                    <Row>
                       
                        {
                        displayData.map( (ele:any)=>
                        <CardEmploye
                        key={ele.employeId}
                        onDropEmploye={()=>OnDropEmploye(ele.employeId,ele.employeFirstName+" "+ele.employeFirstSurname)}
                        onEdit={()=>GoToEditUser(ele.employeId)}
                        employeName={ele.employeFirstName+" "+ele.employeFirstSurname}
                        employeNumberId={ele.employeNumberId}
                        employTypeId={ele.employeIdType}
                        />
                        )     
                        }
                      
                       
                    </Row>

                </div>

            </Col>

            <Col  sm={12} xs={12} md={12} >
                        {
                            !searchValue &&
                            <div className='pagination-container' >
                                <Pagination
                                    onChange={OnChangePagination}
                                    defaultCurrent={1}
                                    pageSize={10}
                                    total={dataSet.length} />
                            </div>
                        }
                   

            </Col>
           </Row>
       </Container>
       </>
    )

}