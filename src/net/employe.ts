import {EmployeRequest} from "./index";

export const GetAllEmployes = ()=>{

  return   EmployeRequest.get("/getAll");

}
export const GetEmployeById = (employeId:number)=>{

  return EmployeRequest.get("byId/"+employeId);

}
export const DropEmploye = (employeId:number)=>{

  return EmployeRequest.get("dropEmploye/"+employeId);

}

export const EditEmploye = (employeData:any)=>{

    return   EmployeRequest.post("/EditUser",employeData);
  
}
export const CreateEmploye = (employeData:any)=>{

    return EmployeRequest.post("/create",employeData);
}
export const GetSuggestMail = (employeNames:any)=>{

  return EmployeRequest.post("/suggestMail",employeNames);
}

